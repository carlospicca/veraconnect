/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.webdav;

import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(reference="DAV:")
@Root(strict=false)
public class DavMultistatus {
	@ElementList(inline=true,entry="response",required=false)
	List<DavResponse> response;
}
