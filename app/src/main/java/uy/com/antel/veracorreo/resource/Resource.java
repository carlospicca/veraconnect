/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.resource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents a resource that can be contained in a LocalCollection or RemoteCollection
 * for synchronization by WebDAV.
 */
@ToString
public abstract class Resource {
	@Getter @Setter protected String name, ETag;
	@Getter @Setter protected String uid;
	@Getter protected long localID;

	
	public Resource(String name, String ETag) {
		this.name = name;
		this.ETag = ETag;
	}
	
	public Resource(long localID, String name, String ETag) {
		this(name, ETag);
		this.localID = localID;
	}
	
	/** initializes UID and remote file name (required for first upload) */
	public abstract void initialize();
	
	/** fills the resource data from an input stream (for instance, .vcf file for Contact) */
	public abstract void parseEntity(InputStream entity) throws IOException, InvalidResourceException;
	/** writes the resource data to an output stream (for instance, .vcf file for Contact) */
	public abstract ByteArrayOutputStream toEntity() throws IOException;
}
