/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.webdav;

public class DavException extends Exception {
	private static final long serialVersionUID = -2118919144443165706L;
	
	final private static String prefix = "Invalid DAV response: ";
	
	/* used to indiciate DAV protocol errors */
	
	
	public DavException(String message) {
		super(prefix + message);
	}
	
	public DavException(String message, Throwable ex) {
		super(prefix + message, ex);
	}
}
