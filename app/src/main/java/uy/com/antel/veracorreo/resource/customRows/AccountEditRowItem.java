/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.resource.customRows;

/**
 * Created by carlospicca on 8/14/14.
 */
public class AccountEditRowItem {
    private String name;
    private int type;
    private int position;
    private int error;

    public AccountEditRowItem(String name, int type, int position) {
        super();
        this.name = name;
        this.type = type;
        this.position = position;
        this.error = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
