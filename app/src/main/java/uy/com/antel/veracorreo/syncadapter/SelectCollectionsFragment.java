/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.syncadapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ListFragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import uy.com.antel.veracorreo.Constants;
import uy.com.antel.veracorreo.R;
import uy.com.antel.veracorreo.activities.MainActivity;
import uy.com.antel.veracorreo.resource.LocalCalendar;
import uy.com.antel.veracorreo.resource.LocalStorageException;
import uy.com.antel.veracorreo.resource.ServerInfo;

public class SelectCollectionsFragment extends ListFragment {
	public static final String KEY_SERVER_INFO = "server_info";

    public static final String NAME_ACCOUNT = "name_account";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = super.onCreateView(inflater, container, savedInstanceState);
		setHasOptionsMenu(true);
		return v;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		setListAdapter(null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		final ListView listView = getListView();
		listView.setPadding(20, 30, 20, 30);
		
		View header = getActivity().getLayoutInflater().inflate(R.layout.select_collections_header, null);
		listView.addHeaderView(header);
		
		final ServerInfo serverInfo = (ServerInfo)getArguments().getSerializable(KEY_SERVER_INFO);
		final SelectCollectionsAdapter adapter = new SelectCollectionsAdapter(view.getContext(), serverInfo);
		setListAdapter(adapter);
		
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int itemPosition = position - 1;	// one list header view at pos. 0
				if (adapter.getItemViewType(itemPosition) == SelectCollectionsAdapter.TYPE_ADDRESS_BOOKS_ROW) {
					// unselect all other address books
					for (int pos = 1; pos <= adapter.getNAddressBooks(); pos++)
						if (pos != itemPosition)
							listView.setItemChecked(pos + 1, false);
				}
				
				getActivity().invalidateOptionsMenu();
			}
		});
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.select_collections, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.next:
			ServerInfo serverInfo = (ServerInfo)getArguments().getSerializable(KEY_SERVER_INFO);
			
			// synchronize only selected collections
			for (ServerInfo.ResourceInfo addressBook : serverInfo.getAddressBooks())
				addressBook.setEnabled(false);
			for (ServerInfo.ResourceInfo calendar : serverInfo.getCalendars())
				calendar.setEnabled(false);
			
			ListAdapter adapter = getListView().getAdapter();
			for (long id : getListView().getCheckedItemIds()) {
				int position = (int)id + 1;		// +1 because header view is inserted at pos. 0 
				ServerInfo.ResourceInfo info = (ServerInfo.ResourceInfo)adapter.getItem(position);
				info.setEnabled(true);
			}
			
			// pass to "account details" fragment
//			AccountDetailsFragment accountDetails = new AccountDetailsFragment();
//			Bundle arguments = new Bundle();
//			arguments.putSerializable(SelectCollectionsFragment.KEY_SERVER_INFO, serverInfo);
//			accountDetails.setArguments(arguments);
//
//			getFragmentManager().beginTransaction()
//				.replace(R.id.fragment_container, accountDetails)
//				.addToBackStack(null)
//				.commitAllowingStateLoss();

            addAccount();
			break;
		default:
			return false;
		}
		return true;
	}
	
	
	// input validation
	
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		boolean ok = false;
		try {
			ok = getListView().getCheckedItemCount() > 0;
		} catch(IllegalStateException e) {
		}
		MenuItem item = menu.findItem(R.id.next);
		item.setEnabled(ok);
	}

    void addAccount() {
        ServerInfo serverInfo = (ServerInfo)getArguments().getSerializable(KEY_SERVER_INFO);
        String accountName = getArguments().getSerializable(NAME_ACCOUNT).toString();

        AccountManager accountManager = AccountManager.get(getActivity());
        Account account = new Account(accountName, Constants.ACCOUNT_TYPE);
        Bundle userData = AccountSettings.createBundle(serverInfo);

        boolean syncContacts = false;
        for (ServerInfo.ResourceInfo addressBook : serverInfo.getAddressBooks())
            if (addressBook.isEnabled()) {
                ContentResolver.setIsSyncable(account, ContactsContract.AUTHORITY, 1);
                syncContacts = true;
                continue;
            }
        if (syncContacts) {
            ContentResolver.setIsSyncable(account, ContactsContract.AUTHORITY, 1);
            ContentResolver.setSyncAutomatically(account, ContactsContract.AUTHORITY, true);
        } else
            ContentResolver.setIsSyncable(account, ContactsContract.AUTHORITY, 0);

        if (accountManager.addAccountExplicitly(account, serverInfo.getPassword(), userData)) {
            // account created, now create calendars
            boolean syncCalendars = false;
            for (ServerInfo.ResourceInfo calendar : serverInfo.getCalendars())
                if (calendar.isEnabled())
                    try {
                        LocalCalendar.create(account, getActivity().getContentResolver(), calendar);
                        syncCalendars = true;
                    } catch (LocalStorageException e) {
                        Toast.makeText(getActivity(), "Couldn't create calendar(s): " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
            if (syncCalendars) {
                ContentResolver.setIsSyncable(account, CalendarContract.AUTHORITY, 1);
                ContentResolver.setSyncAutomatically(account, CalendarContract.AUTHORITY, true);
            } else
                ContentResolver.setIsSyncable(account, CalendarContract.AUTHORITY, 0);

            getActivity().finish();
            Toast.makeText(getActivity(), getString(R.string.done_message), Toast.LENGTH_LONG).show();

            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
        } else
            Toast.makeText(getActivity(), "Couldn't create account (account with this name already existing?)", Toast.LENGTH_LONG).show();
    }
}
