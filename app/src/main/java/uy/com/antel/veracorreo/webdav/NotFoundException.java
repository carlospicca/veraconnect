/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.webdav;

import org.apache.http.HttpStatus;

public class NotFoundException extends HttpException {
	private static final long serialVersionUID = 1565961502781880483L;
	
	public NotFoundException(String reason) {
		super(HttpStatus.SC_NOT_FOUND, reason);
	}
}
