/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.webdav;

import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="href")
@Namespace(prefix="D",reference="DAV:")
public class DavHref {
	@Text
	String href;
	
	DavHref() {
	}
	
	public DavHref(String href) {
		this.href = href;
	}
}
