/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.webdav;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict=false,name="propstat")
public class DavPropstat {
	@Element
	DavProp prop;
	
	@Element
	String status;
}
