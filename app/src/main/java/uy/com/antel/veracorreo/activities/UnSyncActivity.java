/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import uy.com.antel.veracorreo.Constants;
import uy.com.antel.veracorreo.R;
import uy.com.antel.veracorreo.resource.UnSyncRawContact;
import uy.com.antel.veracorreo.resource.UnSyncRawEmail;
import uy.com.antel.veracorreo.resource.UnSyncRawIm;
import uy.com.antel.veracorreo.resource.UnSyncRawNote;
import uy.com.antel.veracorreo.resource.UnSyncRawOrganization;
import uy.com.antel.veracorreo.resource.UnSyncRawPhone;
import uy.com.antel.veracorreo.resource.UnSyncRawPostal;
import uy.com.antel.veracorreo.resource.UnSyncRawWeb;
import uy.com.antel.veracorreo.resource.customRows.UnSyncContactRowItem;
import uy.com.antel.veracorreo.syncadapter.adapters.UnSyncContactRowAdapter;

/**
 * Created by carlospicca on 8/17/14.
 */
public class UnSyncActivity extends Activity implements AdapterView.OnItemClickListener {

    public float counterP;
    public float totalP;
    ProgressDialog progressBar;
    private ArrayList<UnSyncRawContact> contacts;
    private ArrayList<UnSyncRawContact> selectedContacts;
    private ListView listView;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();
    private UnSyncContactRowAdapter adapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.unsync_contacts_activity, menu);
        return true;
    }

    public void showWebsite(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(Constants.WEB_URL_HELP));
        startActivity(intent);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (contacts != null && contacts.size() <= 0)
            menu.getItem(1).setEnabled(false);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
//        setProgressBarIndeterminateVisibility(true);

        setContentView(R.layout.unsync_contact);

        selectedContacts = new ArrayList<UnSyncRawContact>();
//        if (getIntent().getSerializableExtra("CONTACTS") == null)
//            contacts = new ArrayList<UnSyncRawContact>();
//        else
//            contacts = (ArrayList<UnSyncRawContact>) getIntent().getSerializableExtra("CONTACTS");

//        contacts = getUnSyncContacts();
//
//        // 1. pass context and data to the custom adapter
//        UnSyncContactRowAdapter adapter = new UnSyncContactRowAdapter(UnSyncActivity.this, generateData());
//
//        // 2. Get ListView from activity_main.xml
//        listView = (ListView) findViewById(R.id.list);
//
//        listView.setOnItemClickListener(UnSyncActivity.this);
//
//        // 3. setListAdapter
//        listView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        new Thread(new Runnable() {
            public void run() {
                // 1. pass context and data to the custom adapter
                adapter = new UnSyncContactRowAdapter(UnSyncActivity.this, generateData());

                // 2. Get ListView from activity_main.xml
                listView = (ListView) findViewById(R.id.list);

                listView.setOnItemClickListener(UnSyncActivity.this);

                // 3. setListAdapter
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(adapter);
                    }
                });

                contacts = getUnSyncContacts();
            }
        }).start();
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
        UnSyncContactRowItem row = (UnSyncContactRowItem) adapter.getItemAtPosition(position);
        ImageView checkBoxImageView = (ImageView) v.findViewById(R.id.unsync_contact_item_image);

        if (row.getType() == 0) {
            if (!row.isSelected()) {
                selectedContacts.add(contacts.get(position - 1));
                checkBoxImageView.setImageResource(R.drawable.checkbox_checked);
                row.setSelected(true);
            } else {
                selectedContacts.remove(contacts.get(position - 1));
                checkBoxImageView.setImageResource(R.drawable.checkbox_empty);
                row.setSelected(false);
            }
        } else if (row.getType() == -1)
            selectAllRows(row);

        Log.i("UN_SYNC_CONTACT_ACTIVITY", row.getName() + row.getType());
    }

    private void selectAllRows(UnSyncContactRowItem row) {
        UnSyncContactRowAdapter lAdapter = (UnSyncContactRowAdapter) listView.getAdapter();
        ArrayList<UnSyncContactRowItem> items = lAdapter.getItemsArrayList();

        if (!row.isSelected()) {
            selectedContacts.clear();
            for (UnSyncRawContact contact : contacts) {
                selectedContacts.add(contact);
            }
            for (UnSyncContactRowItem item : items) {
                item.setSelected(true);
            }
        } else {
            selectedContacts.clear();
            for (UnSyncContactRowItem item : items) {
                item.setSelected(false);
            }
        }

        lAdapter.notifyDataSetChanged();
    }


    private ArrayList<UnSyncContactRowItem> generateData() {
        ArrayList<UnSyncContactRowItem> items = new ArrayList<UnSyncContactRowItem>();

        if (contacts != null && contacts.size() > 0) {
            items.add(0, new UnSyncContactRowItem(getString(R.string.select_all_rows), -1, 0, 0, "", "", ""));

            for (UnSyncRawContact contact : contacts) {
                items.add(new UnSyncContactRowItem(contact.getContactDisplayName(), 0, -1,
                        contact.getRowId(), contact.getContactId(), contact.getRawAccountName(), contact.getRawAccountType()));
            }
        } else {
            items.add(new UnSyncContactRowItem(getString(R.string.zero_contacts_to_sync), -2, -1,
                    0, "", "", ""));
        }

        return items;
    }

    public void syncContacts(MenuItem item) {
        if (selectedContacts.size() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.dialog_sync_contacts)
                    .setPositiveButton(R.string.sync, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            // prepare for a progress bar dialog
                            showProgressDialog();

                            //reset progress bar status
                            progressBarStatus = 0;
                            counterP = 1;
                            totalP = 100;
                            new Thread(new Runnable() {
                                public void run() {
                                    syncUnSyncContacts();
                                }
                            }).start();

                            new Thread(new Runnable() {
                                public void run() {

                                    while (progressBarStatus < 100) {

                                        // process some tasks
                                        progressBarStatus = Math.round(getProgress());

                                        // Update the progress bar
                                        progressBarHandler.post(new Runnable() {
                                            public void run() {
                                                progressBar.setProgress(progressBarStatus);
                                            }
                                        });
                                    }

                                    // ok, file is downloaded,
                                    if (progressBarStatus >= 100) {

                                        // sleep 2 seconds, so that you can see the 100%
                                        try {
                                            Thread.sleep(2000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }

                                        // close the progress bar dialog
//                                        progressBar.dismiss();
                                        if (UnSyncActivity.this.isDestroyed()) { // or call isFinishing() if min sdk version < 17
                                            return;
                                        }
                                        dismissProgressDialog();
//                                        startSynchronization();

                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                Toast.makeText(UnSyncActivity.this, R.string.synced_contacts, Toast.LENGTH_LONG).show();
                                            }
                                        });

                                        Intent intent = new Intent(getApplicationContext(), EditAccountActivity.class);
                                        intent.putExtra("ACCOUNT_NAME", getIntent().getStringExtra("ACCOUNT_NAME"));
                                        startActivity(intent);
                                    }
                                }
                            }).start();

//                            new Thread(new Runnable() {
//                                public void run() {
//                                    syncUnSyncContacts();
//                                }
//                            }).start();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            builder.create().show();
        } else {
            Toast.makeText(getApplicationContext(), R.string.must_select_conatacts, Toast.LENGTH_LONG).show();
        }
    }

    public float getProgress() {
        Log.d("COUNTER--STATE", String.valueOf(counterP));
        Log.d("TOTAL", String.valueOf(totalP));
        Log.d("PROGRESS", String.valueOf(round((counterP / totalP), 1) * 100));
        return (float) (round((counterP / totalP), 2) * 100);

    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private void startSynchronization() {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
//                settingsBundle.putBoolean(
//                        ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        ContentResolver.requestSync(getAccountByName(), Constants.AUTHORITY_VERA_CONTACTS, settingsBundle);
        ContentResolver.requestSync(getAccountByName(), Constants.AUTHORITY_VERA_CALENDARS, settingsBundle);
    }

    private void syncUnSyncContacts() {

        int counter = 1;
        totalP = selectedContacts.size();

        for (UnSyncRawContact contact : selectedContacts) {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            int rawContactInsertIndex = ops.size();

            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.CONTACT_ID, String.valueOf(contact.getContactId()))
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, getIntent().getStringExtra("ACCOUNT_NAME"))
                    .build());

            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.RawContacts.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.RawContacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contact.getContactDisplayName())
                    .build());

            counter = syncDataContact(counter, contact, ops);
        }
    }

    private int syncDataContact(int counter, UnSyncRawContact contact, ArrayList<ContentProviderOperation> ops) {
        try {

            ContentProviderResult[] results = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            int rawId = Integer.parseInt(results[0].uri.getLastPathSegment());

            ops = new ArrayList<ContentProviderOperation>();

            createPhones(contact, ops, rawId);
            createEmails(contact, ops, rawId);
            createUrls(contact, ops, rawId);
            createNotes(contact, ops, rawId);
            createOrganization(contact, ops, rawId);
            createAddresses(contact, ops, rawId);
            createIms(contact, ops, rawId);

            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            counterP = counter;

        } catch (Exception e) {
            Log.d("ERROR", e.getMessage());
        }
        counter++;
        return counter;
    }

    private void createIms(UnSyncRawContact contact, ArrayList<ContentProviderOperation> ops, int rawId) {
        for (UnSyncRawIm im : contact.getIms()) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, String.valueOf(rawId))
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Im.DATA, im.getName())
                    .withValue(ContactsContract.CommonDataKinds.Im.TYPE, im.getType())
                    .withValue(ContactsContract.CommonDataKinds.Im.PROTOCOL, im.getProtocol())
                    .build());
        }
    }

    private void createAddresses(UnSyncRawContact contact, ArrayList<ContentProviderOperation> ops, int rawId) {
        for (UnSyncRawPostal address : contact.getAddresses()) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, String.valueOf(rawId))
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredPostal.TYPE, address.getType())
                    .withValue(ContactsContract.CommonDataKinds.StructuredPostal.POBOX, address.getPobox())
                    .withValue(ContactsContract.CommonDataKinds.StructuredPostal.STREET, address.getStreet())
                    .withValue(ContactsContract.CommonDataKinds.StructuredPostal.CITY, address.getCity())
                    .withValue(ContactsContract.CommonDataKinds.StructuredPostal.REGION, address.getRegion())
                    .withValue(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE, address.getPostcode())
                    .withValue(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY, address.getCountry())
                    .withValue(ContactsContract.CommonDataKinds.StructuredPostal.NEIGHBORHOOD, address.getNeighborhood())
                    .build());
        }
    }

    private void createOrganization(UnSyncRawContact contact, ArrayList<ContentProviderOperation> ops, int rawId) {
        UnSyncRawOrganization organization = contact.getOrganization();
        if (organization != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, String.valueOf(rawId))
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, organization.getCompany())
                    .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, organization.getTitle())
                    .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, organization.getType())
                    .build());
        }
    }

    private void createNotes(UnSyncRawContact contact, ArrayList<ContentProviderOperation> ops, int rawId) {
        for (UnSyncRawNote note : contact.getNotes()) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, String.valueOf(rawId))
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Note.NOTE, note.getNote())
                    .build());
        }
    }

    private void createUrls(UnSyncRawContact contact, ArrayList<ContentProviderOperation> ops, int rawId) {
        for (UnSyncRawWeb url : contact.getUrls()) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, String.valueOf(rawId))
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Website.URL, url.getUrl())
                    .withValue(ContactsContract.CommonDataKinds.Website.TYPE, url.getUrlType())
                    .build());
        }
    }

    private void createEmails(UnSyncRawContact contact, ArrayList<ContentProviderOperation> ops, int rawId) {
        for (UnSyncRawEmail email : contact.getEmails()) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, String.valueOf(rawId))
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, email.getAddress())
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, email.getAddressType())
                    .build());
        }
    }

    private void createPhones(UnSyncRawContact contact, ArrayList<ContentProviderOperation> ops, int rawId) {
        for (UnSyncRawPhone phone : contact.getPhones()) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, String.valueOf(rawId))
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone.getNumber())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, phone.getType())
                    .build());
        }
    }

    private Account getAccountByName() {
        AccountManager am = AccountManager.get(getApplicationContext());
        Account[] accounts = am.getAccountsByType(Constants.ACCOUNT_TYPE);

        for (Account account : accounts) {
            if (account.name.equals(getIntent().getStringExtra("ACCOUNT_NAME"))) {
                return account;
            }
        }

        return null;
    }

    private ArrayList<UnSyncRawContact> getUnSyncContacts() {

        ArrayList<UnSyncRawContact> contacts = new ArrayList<UnSyncRawContact>();
        ArrayList<String> allContacts = new ArrayList<String>();

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        Cursor cSIM = cr.query(ContactsContract.RawContacts.CONTENT_URI,
                null,
                ContactsContract.RawContacts.ACCOUNT_TYPE + "=?",
                new String[]{Constants.ACCOUNT_TYPE}, null
        );

        if (cSIM.getCount() > 0) {
            while (cSIM.moveToNext()) {
                allContacts.add(cSIM.getString(cSIM.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY)));
            }
        }

        int counter = 1;
        totalP = cur.getCount();
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {

//                Log.d("COUNTER", String.valueOf(counterP));

                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));
                int visibleGroup = cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.IN_VISIBLE_GROUP));
                Log.d("NAME", contactName);

                Cursor c = cr.query(ContactsContract.RawContacts.CONTENT_URI,
                        new String[]{ContactsContract.RawContacts._ID, ContactsContract.RawContacts.ACCOUNT_NAME,
                                ContactsContract.RawContacts.ACCOUNT_TYPE},
                        ContactsContract.RawContacts.CONTACT_ID + "=?",
                        new String[]{String.valueOf(id)}, null
                );

                long rawId = 0;
                String type = "";
                String name = "";
                boolean flag = false;
                boolean sFlag = false;
                if (c != null && c.getCount() > 0) {
//                    Log.d("CURSOR", String.valueOf(c.getCount()));
                    if (!isRegisterAccount(contactName.replace(".", ""), allContacts)) {
                        while (c.moveToNext()) {
                            rawId = c.getLong(c.getColumnIndex(ContactsContract.RawContacts._ID));
                            type = c.getString(c.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_TYPE));
                            name = c.getString(c.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_NAME));

                            if (type != null && name != null) {
//                            Log.d("CURSOR", type);
//                            Log.d("CURSOR", name);
                                if (type.equals(Constants.ACCOUNT_TYPE) && name.equals(getIntent().getStringExtra("ACCOUNT_NAME")))
                                    sFlag = true;
                                if (visibleGroup == 1 && !type.equals(Constants.ACCOUNT_TYPE)
                                        && !name.equals(getIntent().getStringExtra("ACCOUNT_NAME")))
                                    flag = true;

                            } else {
                                flag = true;
//                            Log.d("CURSOR", "No posee");
//                            Log.d("CURSOR", "No name");
                            }
                        }
                    }
                }

                if (!sFlag && flag) {
                    UnSyncRawContact contact = new UnSyncRawContact(rawId, id, type, name, contactName);

                    setUpPhones(contact);
                    setUpEmails(contact);
                    setUpUrls(contact);
                    setUpNotes(contact);
                    setUpOrganization(contact);
                    setUpIms(contact);
                    setUpAddresses(contact);

                    contacts.add(contact);
                }

                counterP = counter;
                counter++;
                c.close();
            }
        }

        cur.close();

        Collections.sort(contacts, new Comparator<UnSyncRawContact>() {
            public int compare(UnSyncRawContact v1, UnSyncRawContact v2) {
                return v1.getContactDisplayName().compareToIgnoreCase(v2.getContactDisplayName());
            }
        });

        this.contacts = contacts;
        final ArrayList<UnSyncContactRowItem> data = generateData();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((UnSyncContactRowAdapter) listView.getAdapter()).clear();
                int c = 0;
                for (UnSyncContactRowItem item : data) {
                    ((UnSyncContactRowAdapter) listView.getAdapter()).insert(item, c);
                    c++;
                }
                ((UnSyncContactRowAdapter) listView.getAdapter()).notifyDataSetChanged();
            }
        });

        return contacts;
    }

    private boolean isRegisterAccount(String name, ArrayList<String> accountsRegister) {
        if (accountsRegister != null) {
            for (String accountName : accountsRegister) {
                if (accountName.equals(name)) return true;
            }
        }

        return false;
    }

    private void setUpPhones(UnSyncRawContact contact) {

        Cursor pCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                new String[]{contact.getContactId()},
                null);

        if (pCur.getCount() > 0) {
            while (pCur.moveToNext()) {
                String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                int contactNumberType = pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                if (contactNumber != null && !contactNumber.equals("") && contactNumberType >= 0)
                    contact.addPhone(new UnSyncRawPhone(contactNumber, contactNumberType));
//                Log.d("PHONE", contactNumber);
//                Log.d("PHONE", String.valueOf(contactNumberType));
            }
        }
        pCur.close();
    }

    private void setUpEmails(UnSyncRawContact contact) {

        Cursor eCur = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{contact.getContactId()},
                null);

        if (eCur.getCount() > 0) {
            while (eCur.moveToNext()) {
                String address = eCur.getString(eCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                int addressType = eCur.getInt(eCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                if (address != null && !address.equals("") && addressType >= 0)
                    contact.addEmail(new UnSyncRawEmail(address, addressType));
//                Log.d("EMAIL", address);
//                Log.d("EMAIL", String.valueOf(addressType));
            }
        }
        eCur.close();
    }

    private void setUpUrls(UnSyncRawContact contact) {

        Cursor wCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Website.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE},
                null);

        if (wCur.getCount() > 0) {
            while (wCur.moveToNext()) {
                String url = wCur.getString(wCur.getColumnIndex(ContactsContract.CommonDataKinds.Website.URL));
                int urlType = wCur.getInt(wCur.getColumnIndex(ContactsContract.CommonDataKinds.Website.TYPE));
                if (url != null && !url.equals("") && urlType >= 0)
                    contact.addUrl(new UnSyncRawWeb(url, urlType));
//                Log.d("URL", url);
//                Log.d("URL", String.valueOf(urlType));
            }
        }
        wCur.close();
    }

    private void setUpNotes(UnSyncRawContact contact) {

        Cursor nCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Note.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE},
                null);

        if (nCur.getCount() > 0) {
            while (nCur.moveToNext()) {
                String note = nCur.getString(nCur.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
                if (note != null && !note.equals(""))
                    contact.addNote(new UnSyncRawNote(note));
//                Log.d("NOTE", note);
            }
        }
        nCur.close();
    }

    private void setUpOrganization(UnSyncRawContact contact) {

        Cursor oCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Organization.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE},
                null);

        if (oCur.getCount() > 0) {
            while (oCur.moveToNext()) {
                String company = oCur.getString(oCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY));
                String title = oCur.getString(oCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
                int organizationType = oCur.getInt(oCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TYPE));
                if (company != null && title != null && !company.equals("") && !title.equals("") && organizationType >= 0)
                    contact.setOrganization(new UnSyncRawOrganization(company, organizationType, title));
//                Log.d("ORGANIZATION", company);
//                Log.d("ORGANIZATION", organizationType);
//                Log.d("ORGANIZATION", title);

            }
        }
        oCur.close();
    }

    private void setUpAddresses(UnSyncRawContact contact) {

        Cursor aCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE},
                null);

        if (aCur.getCount() > 0) {
            while (aCur.moveToNext()) {
                String pobox = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POBOX));
                String street = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                String city = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                String region = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
                String postcode = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
                String country = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
                String neighborhood = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.NEIGHBORHOOD));
                int addressType = aCur.getInt(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));

                if (pobox == null) pobox = "";
                if (street == null) street = "";
                if (city == null) city = "";
                if (region == null) region = "";
                if (postcode == null) postcode = "";
                if (country == null) country = "";
                if (neighborhood == null) neighborhood = "";

                if (addressType >= 0)
                    contact.addAddress(new UnSyncRawPostal(addressType, pobox, street, city, region,
                            postcode, country, neighborhood));
            }
        }
        aCur.close();
    }

    private void setUpIms(UnSyncRawContact contact) {

        Cursor iCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Im.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE},
                null);

        Log.d("IMS", "---------- THERE IS " + iCur.getCount() + " ------------");
        if (iCur.getCount() > 0) {
            while (iCur.moveToNext()) {
                String name = iCur.getString(iCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA));
                String protocol = iCur.getString(iCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.PROTOCOL));
                int imType = iCur.getInt(iCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.TYPE));

                if (protocol == null) protocol = "";

//                if (name != null && !name.equals("") && imType >= 0)

                // verificar validador con rodrigo tal vez este afectando al ace3
                Log.d("IMS", "---------- BEGIN TEST FOR IMS ------------");
                Log.d("IMS-CONTACT", contact.getContactId());
                if (name != null || name.equals(""))
                    Log.d("IMS-NAME", "VACIO O NULO");
                else
                    Log.d("IMS-NAME", name);
                if (imType >= 0)
                    Log.d("IMS-TYPE", String.valueOf(imType));
                else
                    Log.d("IMS-TYPE", "TYPE MENOR QUE CERO (0)");
                Log.d("IMS", "---------- END TEST FOR IMS ------------");

                if (name != null && !name.equals(""))
                    contact.addIm(new UnSyncRawIm(name, imType, protocol));
            }
        }
        Log.d("IMS", "---------- THERE IS NO IMS AVAIBALE ------------");
        iCur.close();
    }

    private void showProgressDialog() {
        if (progressBar == null) {
            progressBar = new ProgressDialog(UnSyncActivity.this);
            progressBar.setCancelable(false);
            progressBar.setMessage(getString(R.string.prepare_un_sync_contacts));
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.setProgressNumberFormat("");
        }
        progressBar.show();
    }

    private void dismissProgressDialog() {
        if (progressBar != null && progressBar.isShowing()) {
            progressBar.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

}

