/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.webdav;

import java.util.List;

import lombok.Getter;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict=false)
public class DavResponse {
	@Element
	@Getter DavHref href;

	@ElementList(inline=true)
	@Getter List<DavPropstat> propstat;
}
