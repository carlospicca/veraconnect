/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import java.util.ArrayList;
import java.util.HashSet;

import uy.com.antel.veracorreo.Constants;
import uy.com.antel.veracorreo.R;
import uy.com.antel.veracorreo.resource.customRows.AccountRowItem;
import uy.com.antel.veracorreo.syncadapter.EnterCredentialsFragment;
import uy.com.antel.veracorreo.syncadapter.SelectCollectionsFragment;
import uy.com.antel.veracorreo.syncadapter.adapters.AccountRowAdapter;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener {

    public static final String PREFS_NAME = "VeraPrefFile";
    static boolean wActive = false;
    private ListView listView;
    private BroadcastReceiver httpErrorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            savePreferences(1);
            restorePreferences();

            Toast.makeText(getApplicationContext(), R.string.wrong_password, Toast.LENGTH_LONG).show();
        }
    };
    private BroadcastReceiver noHttpErrorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            savePreferences(0);
            restorePreferences();

//            Toast.makeText(getApplicationContext(), R.string.correct_password, Toast.LENGTH_LONG).show();
        }
    };

    private void savePreferences(int error) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        HashSet<String> accounts = new HashSet<String>();
        HashSet<String> accountErrors = (HashSet<String>) settings.getStringSet("accountProblems", null);
        boolean flag = true;
        String errorValue = "N";
        if (error == 1) errorValue = "Y";

        if (accountErrors != null) {
            for (String account : accountErrors) {
                String accountName = account.substring(0, account.indexOf("|"));
                String action = account.substring(account.indexOf("|") + 1, account.length());
                AccountRowAdapter lAdapter = (AccountRowAdapter) listView.getAdapter();
                ArrayList<AccountRowItem> items = lAdapter.getItemsArrayList();
                if (items != null) {
                    for (AccountRowItem item : items) {
                        if (item.getName().equals(accountName)) {

                            flag = false;
                            accounts.add(accountName + "|" + errorValue);
                        } else
                            accounts.add(accountName + "|" + action);
                    }
                }
            }
        }
        if (flag) accounts.add(getIntent().getStringExtra("ACCOUNT_NAME") + "|" + errorValue);

        editor.putStringSet("accountProblems", accounts);

        // Commit the edits!
        editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity, menu);
        return true;
    }

    public void showWebsite(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(Constants.WEB_URL_HELP));
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        restorePreferences();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(httpErrorReceiver, new IntentFilter("http-error"));
        LocalBroadcastManager.getInstance(this).registerReceiver(noHttpErrorReceiver, new IntentFilter("http-no-error"));

        setContentView(R.layout.new_main_activity);

        // 1. pass context and data to the custom adapter
        AccountRowAdapter adapter = new AccountRowAdapter(this, generateData(), getSharedPreferences(PREFS_NAME, 0));

        // 2. Get ListView from activity_main.xml
        listView = (ListView) findViewById(R.id.list);

        listView.setOnItemClickListener(this);

        // 3. setListAdapter
        listView.setAdapter(adapter);

        // Restore preferences
        restorePreferences();
    }

    private void restorePreferences() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        HashSet<String> accountErrors = (HashSet<String>) settings.getStringSet("accountProblems", null);
        AccountRowAdapter lAdapter = (AccountRowAdapter) listView.getAdapter();
        ArrayList<AccountRowItem> items = lAdapter.getItemsArrayList();
        if (accountErrors != null) {
            for (String account : accountErrors) {
                String accountName = account.substring(0, account.indexOf("|"));
                String action = account.substring(account.indexOf("|") + 1, account.length());

                if (items != null) {
                    for (AccountRowItem item : items) {
                        if (item.getName().equals(accountName)) {
                            if (action.equals("Y")) {
                                item.setError(1); //row with 'change pass' action
                            } else {
                                item.setError(0); //row with account name
                            }
                        }
                    }
                }
            }

        }
        lAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
        AccountRowItem row = (AccountRowItem) adapter.getItemAtPosition(position);

        if (row.getType() == 0) {
            setContentView(R.layout.add_account);

            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new EnterCredentialsFragment(), "enter_credentials")
                    .commit();
        } else {
            Intent intent = new Intent(this, EditAccountActivity.class);
            intent.putExtra("ACCOUNT_NAME", row.getName());
            startActivity(intent);
        }

        Log.i("sassss", row.getName() + row.getType());
    }


    private ArrayList<AccountRowItem> generateData() {
        ArrayList<AccountRowItem> items = new ArrayList<AccountRowItem>();

        AccountManager accountManager = AccountManager.get(this);
        Account[] accounts = accountManager.getAccountsByType(Constants.ACCOUNT_TYPE);

        for (Account account : accounts) {
            items.add(new AccountRowItem(account.name, 1));
        }
        items.add(new AccountRowItem(getString(R.string.new_account), 0));

        return items;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            SelectCollectionsFragment fragment = (SelectCollectionsFragment) getFragmentManager()
                    .findFragmentByTag("SELECT_COLLECTIONS");
            Intent intent = null;

            if (fragment != null && fragment.isVisible()) {
                Log.d("KEY-DOWN", "BACK FROM COLLECTIONS");
                intent = new Intent(this, AddAccountActivity.class);
            } else
                intent = new Intent(this, MainActivity.class);

            if (!wActive)
                startActivity(intent);
            else
                moveTaskToBack(true);

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onStart() {
        super.onStart();
        wActive = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        wActive = false;
    }

}
