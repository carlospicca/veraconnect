/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.webdav;

public class DavIncapableException extends DavException {
	private static final long serialVersionUID = -7199786680939975667L;
	
	/* used to indicate that the server doesn't support DAV */
	
	public DavIncapableException(String msg) {
		super(msg);
	}
}
