/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.webdav;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Namespace(reference="DAV:")
@Root(name="propfind")
public class DavPropfind {
	@Element(required=false)
	protected DavProp prop;
}
