/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.webdav;

import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

@Root(name="addressbook-multiget")
@NamespaceList({
	@Namespace(reference="DAV:"),
	@Namespace(prefix="CD",reference="urn:ietf:params:xml:ns:carddav")
})
@Namespace(prefix="CD",reference="urn:ietf:params:xml:ns:carddav")
public class DavAddressbookMultiget extends DavMultiget {
}
