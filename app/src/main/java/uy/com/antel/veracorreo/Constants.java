/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo;

public class Constants {
	public static final String
		APP_VERSION = "1.0",
//		ACCOUNT_TYPE = "carlospicca.com.veracorreo",
		ACCOUNT_TYPE = "uy.com.antel.veracorreo",
		WEB_URL_HELP = "https://correo.vera.com.uy",
        URL_VERA_SERVER = "https://correo.vera.com.uy",

		SETTING_DISABLE_COMPRESSION = "disable_compression",
		SETTING_NETWORK_LOGGING = "network_logging",

        AUTHORITY_VERA_CALENDARS = "com.android.calendar",
        AUTHORITY_VERA_CONTACTS = "com.android.contacts";

}
