/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.resource.customRows;

/**
 * Created by carlospicca on 8/17/14.
 */
public class UnSyncContactRowItem {
    private String name;
    private int type;
    private int position;
    private long rowId;
    private String contactId;
    private String rawAccountName;
    private String rawAccountType;
    private boolean isSelected;

    public UnSyncContactRowItem(String name, int type, int position, long rowId, String contactId, String rawAccountName, String rawAccountType) {
        super();
        this.name = name;
        this.type = type;
        this.position = position;
        this.rowId = rowId;
        this.contactId = contactId;
        this.rawAccountName = rawAccountName;
        this.rawAccountType = rawAccountType;
        this.isSelected = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getRawAccountName() {
        return rawAccountName;
    }

    public void setRawAccountName(String rawAccountName) {
        this.rawAccountName = rawAccountName;
    }

    public String getRawAccountType() {
        return rawAccountType;
    }

    public void setRawAccountType(String rawAccountType) {
        this.rawAccountType = rawAccountType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
