/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.resource;

import java.io.Serializable;

/**
 * Created by carlospicca on 8/22/14.
 */
public class UnSyncRawEmail implements Serializable {
    private String address;
    private int addressType;

    public UnSyncRawEmail(String address, int addressType) {
        this.address = address;
        this.addressType = addressType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAddressType() {
        return addressType;
    }

    public void setAddressType(int addressType) {
        this.addressType = addressType;
    }
}