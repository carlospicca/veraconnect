/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.resource;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by carlospicca on 8/17/14.
 */
public class UnSyncRawContact implements Serializable {
    public static final long serialVersionUID = 97646;
    private long rawId;
    private String contactId;
    private String rawAccountName;
    private String rawAccountType;
    private String contactDisplayName;
    private ArrayList<UnSyncRawPhone> phones;
    private ArrayList<UnSyncRawEmail> emails;
    private ArrayList<UnSyncRawWeb> urls;
    private ArrayList<UnSyncRawNote> notes;
    private UnSyncRawOrganization organization;
    private ArrayList<UnSyncRawIm> ims;
    private ArrayList<UnSyncRawPostal> addresses;

    public UnSyncRawContact(long rowId, String contactId, String rawAccountName, String rawAccountType, String contactDisplayName) {
        this.rawId = rowId;
        this.contactId = contactId;
        this.rawAccountName = rawAccountName;
        this.rawAccountType = rawAccountType;
        this.contactDisplayName = contactDisplayName;
        this.phones = new ArrayList<UnSyncRawPhone>();
        this.emails = new ArrayList<UnSyncRawEmail>();
        this.urls = new ArrayList<UnSyncRawWeb>();
        this.notes = new ArrayList<UnSyncRawNote>();
        this.organization = new UnSyncRawOrganization(null, 0, null);
        this.ims = new ArrayList<UnSyncRawIm>();
        this.addresses = new ArrayList<UnSyncRawPostal>();
    }

    public long getRowId() {
        return rawId;
    }

    public void setRowId(long rowId) {
        this.rawId = rowId;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getRawAccountName() {
        return rawAccountName;
    }

    public void setRawAccountName(String rawAccountName) {
        this.rawAccountName = rawAccountName;
    }

    public String getRawAccountType() {
        return rawAccountType;
    }

    public void setRawAccountType(String rawAccountType) {
        this.rawAccountType = rawAccountType;
    }

    public String getContactDisplayName() {
        return contactDisplayName;
    }

    public void setContactDisplayName(String contactDisplayName) {
        this.contactDisplayName = contactDisplayName;
    }

    public long getRawId() {
        return rawId;
    }

    public void setRawId(long rawId) {
        this.rawId = rawId;
    }


    public ArrayList<UnSyncRawPhone> getPhones() {
        return phones;
    }

    public void addPhone(UnSyncRawPhone phone) {
        phones.add(phone);
    }

    public UnSyncRawPhone getPhone(int index) {
        return phones.get(index);
    }

    public void removePhone(UnSyncRawPhone phone) {
        phones.remove(phone);
    }


    public ArrayList<UnSyncRawEmail> getEmails() {
        return emails;
    }

    public void addEmail(UnSyncRawEmail email) {
        emails.add(email);
    }

    public UnSyncRawEmail getEmail(int index) {
        return emails.get(index);
    }

    public void removeEmail(UnSyncRawEmail email) {
        emails.remove(email);
    }


    public ArrayList<UnSyncRawWeb> getUrls() {
        return urls;
    }

    public void addUrl(UnSyncRawWeb url) {
        urls.add(url);
    }

    public UnSyncRawWeb getUrl(int index) {
        return urls.get(index);
    }

    public void removeUrl(UnSyncRawWeb url) {
        urls.remove(url);
    }


    public ArrayList<UnSyncRawNote> getNotes() {
        return notes;
    }

    public void addNote(UnSyncRawNote note) {
        notes.add(note);
    }

    public UnSyncRawNote getNote(int index) {
        return notes.get(index);
    }

    public void removeNote(UnSyncRawNote note) {
        notes.remove(note);
    }


    public UnSyncRawOrganization getOrganization() {
        return organization;
    }

    public void setOrganization(UnSyncRawOrganization organization) {
        this.organization = organization;
    }


    public ArrayList<UnSyncRawIm> getIms() {
        return ims;
    }

    public void addIm(UnSyncRawIm im) {
        ims.add(im);
    }

    public UnSyncRawIm getIm(int index) {
        return ims.get(index);
    }

    public void removeIm(UnSyncRawIm im) {
        ims.remove(im);
    }


    public ArrayList<UnSyncRawPostal> getAddresses() {
        return addresses;
    }

    public void addAddress(UnSyncRawPostal address) {
        addresses.add(address);
    }

    public UnSyncRawPostal getAddress(int index) {
        return addresses.get(index);
    }

    public void removeAddress(UnSyncRawPostal address) {
        addresses.remove(address);
    }
}
