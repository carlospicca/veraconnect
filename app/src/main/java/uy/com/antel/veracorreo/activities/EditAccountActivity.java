/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

import uy.com.antel.veracorreo.Constants;
import uy.com.antel.veracorreo.R;
import uy.com.antel.veracorreo.resource.UnSyncRawContact;
import uy.com.antel.veracorreo.resource.UnSyncRawEmail;
import uy.com.antel.veracorreo.resource.UnSyncRawIm;
import uy.com.antel.veracorreo.resource.UnSyncRawNote;
import uy.com.antel.veracorreo.resource.UnSyncRawOrganization;
import uy.com.antel.veracorreo.resource.UnSyncRawPhone;
import uy.com.antel.veracorreo.resource.UnSyncRawPostal;
import uy.com.antel.veracorreo.resource.UnSyncRawWeb;
import uy.com.antel.veracorreo.resource.customRows.AccountEditRowItem;
import uy.com.antel.veracorreo.syncadapter.adapters.AccountEditRowAdapter;

/**
 * Created by carlospicca on 8/14/14.
 */
public class EditAccountActivity extends Activity implements AdapterView.OnItemClickListener {

    public static final String PREFS_NAME = "VeraPrefFile";
    public float counterP;
    public float totalP;
    ProgressDialog progressBar;
    private BroadcastReceiver httpErrorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            savePreferences(1);
            restorePreferences();

            Toast.makeText(getApplicationContext(), R.string.wrong_password, Toast.LENGTH_LONG).show();
        }
    };
    private BroadcastReceiver noHttpErrorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            savePreferences(0);
            restorePreferences();

//            Toast.makeText(getApplicationContext(), R.string.correct_password, Toast.LENGTH_LONG).show();
        }
    };
    private ArrayList<UnSyncRawContact> contacts;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();
    private ListView listView;

    private void savePreferences(int error) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        HashSet<String> accounts = new HashSet<String>();
        HashSet<String> accountErrors = (HashSet<String>) settings.getStringSet("accountProblems", null);
        boolean flag = true;
        String errorValue = "N";
        if (error == 1) errorValue = "Y";

        if (accountErrors != null) {
            for (String account : accountErrors) {
                String accountName = account.substring(0, account.indexOf("|"));
                String action = account.substring(account.indexOf("|") + 1, account.length());
                if (getIntent().getStringExtra("ACCOUNT_NAME") != null && accountName != null &&
                        getIntent().getStringExtra("ACCOUNT_NAME").equals(accountName)) {
                    flag = false;
                    accounts.add(accountName + "|" + errorValue);
                } else
                    accounts.add(accountName + "|" + action);
            }
        }
        if (flag) accounts.add(getIntent().getStringExtra("ACCOUNT_NAME") + "|" + errorValue);

        editor.putStringSet("accountProblems", accounts);

        // Commit the edits!
        editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity, menu);
        return true;
    }

    public void showWebsite(MenuItem item) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(Constants.WEB_URL_HELP));
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocalBroadcastManager.getInstance(this).registerReceiver(httpErrorReceiver, new IntentFilter("http-error"));
        LocalBroadcastManager.getInstance(this).registerReceiver(noHttpErrorReceiver, new IntentFilter("http-no-error"));

        setContentView(R.layout.account_edit);

        // 1. pass context and data to the custom adapter
        AccountEditRowAdapter adapter = new AccountEditRowAdapter(this, generateData());

        // 2. Get ListView from activity_main.xml
        listView = (ListView) findViewById(R.id.list);

        listView.setOnItemClickListener(this);

        // 3. setListAdapter
        listView.setAdapter(adapter);

//        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
//        SharedPreferences.Editor editor = settings.edit();
//        editor.putStringSet("accountProblems", null);
//        editor.commit();

        // Restore preferences
        restorePreferences();
    }

    private void restorePreferences() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        HashSet<String> accountErrors = (HashSet<String>) settings.getStringSet("accountProblems", null);
        if (accountErrors != null) {
            for (String account : accountErrors) {
                String accountName = account.substring(0, account.indexOf("|"));
                String action = account.substring(account.indexOf("|") + 1, account.length());
                if (getIntent().getStringExtra("ACCOUNT_NAME") != null && accountName != null &&
                        getIntent().getStringExtra("ACCOUNT_NAME").equals(accountName)) {
                    AccountEditRowAdapter lAdapter = (AccountEditRowAdapter) listView.getAdapter();
                    ArrayList<AccountEditRowItem> items = lAdapter.getItemsArrayList();

                    if (action.equals("Y")) {
//                        items.get(0).setError(1); //row with account name
                        items.get(2).setError(1); //row with 'change pass' action
                    } else {
//                        items.get(0).setError(0); //row with account name
                        items.get(2).setError(0); //row with 'change pass' action
                    }

                    lAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
        AccountEditRowItem row = (AccountEditRowItem) adapter.getItemAtPosition(position);

        switch (row.getPosition()) {

            case 1:
                //call sync
                callSync();
                break;


            case 2:
                //call change password
                changePassAccount();
                break;


            case 3:
                //call view sync old contacts
                // prepare for a progress bar dialog
//                progressBar = new ProgressDialog(v.getContext());
//                progressBar.setCancelable(false);
//                progressBar.setMessage(getString(R.string.dialog_checking_contacts));
//                progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//                progressBar.setProgress(0);
//                progressBar.setMax(100);
//                progressBar.setProgressNumberFormat("");
//                progressBar.show();
//
//                //reset progress bar status
//                progressBarStatus = 0;
//
//                new Thread(new Runnable() {
//                    public void run() {
//                        counterP = 1;
//                        totalP = 7;
//
//                        while (progressBarStatus < 100) {
//
//                            // process some tasks
//                            progressBarStatus = Math.round(getProgress());
//
//                            // Update the progress bar
//                            progressBarHandler.post(new Runnable() {
//                                public void run() {
//                                    progressBar.setProgress(progressBarStatus);
//                                }
//                            });
//                        }
//
//                        // ok, file is downloaded,
//                        if (progressBarStatus >= 100) {
//
//                            // sleep 2 seconds, so that you can see the 100%
//                            try {
//                                Thread.sleep(2000);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//
//                            // close the progress bar dialog
//                            progressBar.dismiss();
//                            Intent intent = new Intent(EditAccountActivity.this, UnSyncActivity.class);
//
//                            Bundle bundle = new Bundle();
//                            bundle.putSerializable("CONTACTS", contacts);
//                            intent.putExtra("ACCOUNT_NAME", getIntent().getStringExtra("ACCOUNT_NAME"));
//                            intent.putExtras(bundle);
//                            startActivity(intent);
//                        }
//                    }
//                }).start();

                new Thread(new Runnable() {
                    public void run() {
                        Intent intent = new Intent(EditAccountActivity.this, UnSyncActivity.class);
//
                            Bundle bundle = new Bundle();
//                            bundle.putSerializable("CONTACTS", contacts);
                            intent.putExtra("ACCOUNT_NAME", getIntent().getStringExtra("ACCOUNT_NAME"));
                            intent.putExtras(bundle);
                            startActivity(intent);
                    }
                }).start();
                break;


            case 4:
                //delete account
                deleteAccount();
                break;

            default:
                break;
        }

        Log.i("EDIT_ACCOUNT_ACTIVITY", row.getName() + row.getType());
    }

    private void callSync() {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
//                settingsBundle.putBoolean(
//                        ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        ContentResolver.requestSync(getAccountByName(), Constants.AUTHORITY_VERA_CONTACTS, settingsBundle);
        ContentResolver.requestSync(getAccountByName(), Constants.AUTHORITY_VERA_CALENDARS, settingsBundle);

        Toast.makeText(this, R.string.syncing_account, Toast.LENGTH_LONG).show();
    }

    public float getProgress() {
//        Log.d("COUNTER--STATE", String.valueOf(counterP));
//        Log.d("TOTAL", String.valueOf(totalP));
//        Log.d("PROGRESS", String.valueOf(round((counterP / totalP), 1) * 100));
        return (float) (round((counterP / totalP), 2) * 100);

    }


    private ArrayList<AccountEditRowItem> generateData() {
        ArrayList<AccountEditRowItem> items = new ArrayList<AccountEditRowItem>();

        items.add(new AccountEditRowItem(getIntent().getStringExtra("ACCOUNT_NAME"), 1, -1));
        items.add(new AccountEditRowItem(getString(R.string.account_edit_sync), 0, 1));
        items.add(new AccountEditRowItem(getString(R.string.account_edit_change_password), 0, 2));
        items.add(new AccountEditRowItem(getString(R.string.account_edit_sync_contacts), 0, 3));
        items.add(new AccountEditRowItem(getString(R.string.account_edit_delete), 0, 4));

        return items;
    }

    private Account getAccountByName() {
        AccountManager am = AccountManager.get(getApplicationContext());
        Account[] accounts = am.getAccountsByType(Constants.ACCOUNT_TYPE);

        for (Account account : accounts) {
            if (account.name.equals(getIntent().getStringExtra("ACCOUNT_NAME"))) {
                return account;
            }
        }

        return null;
    }

    private void deleteAccount() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_delete_account)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Account accountToRemove = getAccountByName();
                        if (accountToRemove != null) {

                            AccountManager.get(getApplicationContext()).removeAccount(accountToRemove, null, null);

                            Toast.makeText(getApplicationContext(), R.string.account_removed, Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.error_account_removed, Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    private void changePassAccount() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        final View dialogView = inflater.inflate(R.layout.dialog_change_pass, null);
        builder.setView(dialogView)
                // Add action buttons
                .setPositiveButton(R.string.dialog_account_edit_change_password_title, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        final Account accountToChange = getAccountByName();

                        EditText password = (EditText) dialogView.findViewById(R.id.password);

                        AccountManager.get(getApplicationContext()).setPassword(accountToChange, password.getText().toString());

                        Toast.makeText(getApplicationContext(), R.string.password_changed, Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
    }

    private ArrayList<UnSyncRawContact> getUnSyncContacts() {

        ArrayList<UnSyncRawContact> contacts = new ArrayList<UnSyncRawContact>();
        ArrayList<String> allContacts = new ArrayList<String>();

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        Cursor cSIM = cr.query(ContactsContract.RawContacts.CONTENT_URI,
                null,
                ContactsContract.RawContacts.ACCOUNT_TYPE + "=?",
                new String[]{Constants.ACCOUNT_TYPE}, null
        );

        if (cSIM.getCount() > 0) {
            while (cSIM.moveToNext()) {
                allContacts.add(cSIM.getString(cSIM.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY)));
            }
        }

        int counter = 1;
        totalP = cur.getCount();
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {

//                Log.d("COUNTER", String.valueOf(counterP));

                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));
                int visibleGroup = cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.IN_VISIBLE_GROUP));
                Log.d("NAME", contactName);

                Cursor c = cr.query(ContactsContract.RawContacts.CONTENT_URI,
                        new String[]{ContactsContract.RawContacts._ID, ContactsContract.RawContacts.ACCOUNT_NAME,
                                ContactsContract.RawContacts.ACCOUNT_TYPE},
                        ContactsContract.RawContacts.CONTACT_ID + "=?",
                        new String[]{String.valueOf(id)}, null
                );

                long rawId = 0;
                String type = "";
                String name = "";
                boolean flag = false;
                boolean sFlag = false;
                if (c != null && c.getCount() > 0) {
//                    Log.d("CURSOR", String.valueOf(c.getCount()));
                    if (!isRegisterAccount(contactName.replace(".", ""), allContacts)) {
                        while (c.moveToNext()) {
                            rawId = c.getLong(c.getColumnIndex(ContactsContract.RawContacts._ID));
                            type = c.getString(c.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_TYPE));
                            name = c.getString(c.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_NAME));

                            if (type != null && name != null) {
//                            Log.d("CURSOR", type);
//                            Log.d("CURSOR", name);
                                if (type.equals(Constants.ACCOUNT_TYPE) && name.equals(getIntent().getStringExtra("ACCOUNT_NAME")))
                                    sFlag = true;
                                if (visibleGroup == 1 && !type.equals(Constants.ACCOUNT_TYPE)
                                        && !name.equals(getIntent().getStringExtra("ACCOUNT_NAME")))
                                    flag = true;

                            } else {
                                flag = true;
//                            Log.d("CURSOR", "No posee");
//                            Log.d("CURSOR", "No name");
                            }
                        }
                    }
                }

                if (!sFlag && flag) {
                    UnSyncRawContact contact = new UnSyncRawContact(rawId, id, type, name, contactName);

                    setUpPhones(contact);
                    setUpEmails(contact);
                    setUpUrls(contact);
                    setUpNotes(contact);
                    setUpOrganization(contact);
                    setUpIms(contact);
                    setUpAddresses(contact);

                    contacts.add(contact);
                }

                counterP = counter;
                counter++;
                c.close();
            }
        }

        cur.close();

        Collections.sort(contacts, new Comparator<UnSyncRawContact>() {
            public int compare(UnSyncRawContact v1, UnSyncRawContact v2) {
                return v1.getContactDisplayName().compareToIgnoreCase(v2.getContactDisplayName());
            }
        });

        return contacts;
    }

    private boolean isRegisterAccount(String name, ArrayList<String> accountsRegister) {
        if (accountsRegister != null) {
            for (String accountName : accountsRegister) {
                if (accountName.equals(name)) return true;
            }
        }

        return false;
    }

    private void setUpPhones(UnSyncRawContact contact) {

        Cursor pCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                new String[]{contact.getContactId()},
                null);

        if (pCur.getCount() > 0) {
            while (pCur.moveToNext()) {
                String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                int contactNumberType = pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                if (contactNumber != null && !contactNumber.equals("") && contactNumberType >= 0)
                    contact.addPhone(new UnSyncRawPhone(contactNumber, contactNumberType));
//                Log.d("PHONE", contactNumber);
//                Log.d("PHONE", String.valueOf(contactNumberType));
            }
        }
        pCur.close();
    }

    private void setUpEmails(UnSyncRawContact contact) {

        Cursor eCur = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{contact.getContactId()},
                null);

        if (eCur.getCount() > 0) {
            while (eCur.moveToNext()) {
                String address = eCur.getString(eCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                int addressType = eCur.getInt(eCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                if (address != null && !address.equals("") && addressType >= 0)
                    contact.addEmail(new UnSyncRawEmail(address, addressType));
//                Log.d("EMAIL", address);
//                Log.d("EMAIL", String.valueOf(addressType));
            }
        }
        eCur.close();
    }

    private void setUpUrls(UnSyncRawContact contact) {

        Cursor wCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Website.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE},
                null);

        if (wCur.getCount() > 0) {
            while (wCur.moveToNext()) {
                String url = wCur.getString(wCur.getColumnIndex(ContactsContract.CommonDataKinds.Website.URL));
                int urlType = wCur.getInt(wCur.getColumnIndex(ContactsContract.CommonDataKinds.Website.TYPE));
                if (url != null && !url.equals("") && urlType >= 0)
                    contact.addUrl(new UnSyncRawWeb(url, urlType));
//                Log.d("URL", url);
//                Log.d("URL", String.valueOf(urlType));
            }
        }
        wCur.close();
    }

    private void setUpNotes(UnSyncRawContact contact) {

        Cursor nCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Note.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE},
                null);

        if (nCur.getCount() > 0) {
            while (nCur.moveToNext()) {
                String note = nCur.getString(nCur.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
                if (note != null && !note.equals(""))
                    contact.addNote(new UnSyncRawNote(note));
//                Log.d("NOTE", note);
            }
        }
        nCur.close();
    }

    private void setUpOrganization(UnSyncRawContact contact) {

        Cursor oCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Note.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE},
                null);

        if (oCur.getCount() > 0) {
            while (oCur.moveToNext()) {
                String company = oCur.getString(oCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY));
                String title = oCur.getString(oCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
                int organizationType = oCur.getInt(oCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TYPE));
                if (company != null && title != null && !company.equals("") && !title.equals("") && organizationType >= 0)
                    contact.setOrganization(new UnSyncRawOrganization(company, organizationType, title));
//                Log.d("ORGANIZATION", company);
//                Log.d("ORGANIZATION", organizationType);
//                Log.d("ORGANIZATION", title);

            }
        }
        oCur.close();
    }

    private void setUpAddresses(UnSyncRawContact contact) {

        Cursor aCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Note.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE},
                null);

        if (aCur.getCount() > 0) {
            while (aCur.moveToNext()) {
                String pobox = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POBOX));
                String street = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                String city = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                String region = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
                String postcode = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
                String country = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
                String neighborhood = aCur.getString(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.NEIGHBORHOOD));
                int addressType = aCur.getInt(aCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));

                if (pobox == null) pobox = "";
                if (street == null) street = "";
                if (city == null) city = "";
                if (region == null) region = "";
                if (postcode == null) postcode = "";
                if (country == null) country = "";
                if (neighborhood == null) neighborhood = "";

                if (addressType >= 0)
                    contact.addAddress(new UnSyncRawPostal(addressType, pobox, street, city, region,
                            postcode, country, neighborhood));
            }
        }
        aCur.close();
    }

    private void setUpIms(UnSyncRawContact contact) {

        Cursor iCur = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Note.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                new String[]{contact.getContactId(), ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE},
                null);

        if (iCur.getCount() > 0) {
            while (iCur.moveToNext()) {
                String name = iCur.getString(iCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA));
                String protocol = iCur.getString(iCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.PROTOCOL));
                int imType = iCur.getInt(iCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.TYPE));

                if (protocol == null) protocol = "";

                if (name != null && !name.equals("") && imType >= 0)
                    contact.addIm(new UnSyncRawIm(name, imType, protocol));
            }
        }
        iCur.close();
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
