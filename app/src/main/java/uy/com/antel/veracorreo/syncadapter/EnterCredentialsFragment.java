/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.syncadapter;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import uy.com.antel.veracorreo.Constants;
import uy.com.antel.veracorreo.R;
import uy.com.antel.veracorreo.URIUtils;

public class EnterCredentialsFragment extends Fragment implements TextWatcher {
//	String protocol;
	
//	TextView textHttpWarning;
	EditText /*editBaseURL,*/ editUserName, editPassword;
//	CheckBox checkboxPreemptive;
	Button btnNext;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.enter_credentials, container, false);
		
		editUserName = (EditText) v.findViewById(R.id.userName);
		editUserName.addTextChangedListener(this);
		
		editPassword = (EditText) v.findViewById(R.id.password);
		editPassword.addTextChangedListener(this);

        btnNext = (Button) v.findViewById(R.id.next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                prepareDataForQuery();
            }
        });

		// hook into action bar
//		setHasOptionsMenu(true);

		return v;
	}

	 void queryServer() {
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		
		Bundle args = new Bundle();
		
//		String host_path = editBaseURL.getText().toString();
//		args.putString(QueryServerDialogFragment.EXTRA_BASE_URL, URIUtils.sanitize(protocol + host_path));
        args.putString(QueryServerDialogFragment.EXTRA_BASE_URL, URIUtils.sanitize(Constants.URL_VERA_SERVER));
		args.putString(QueryServerDialogFragment.EXTRA_USER_NAME, editUserName.getText().toString());
		args.putString(QueryServerDialogFragment.EXTRA_PASSWORD, editPassword.getText().toString());
//		args.putBoolean(QueryServerDialogFragment.EXTRA_AUTH_PREEMPTIVE, checkboxPreemptive.isChecked());
        args.putBoolean(QueryServerDialogFragment.EXTRA_AUTH_PREEMPTIVE, true);

        DialogFragment dialog = new QueryServerDialogFragment();
		dialog.setArguments(args);
	    dialog.show(ft, QueryServerDialogFragment.class.getName());
	}

	
	// input validation
	public void prepareDataForQuery() {
		boolean ok =
			editUserName.getText().length() > 0 &&
			editPassword.getText().length() > 0;

        if (!ok) {
            Context context = getActivity().getApplicationContext();
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, getString(R.string.user_and_pass_wrong), duration);
            toast.show();
        } else {
            queryServer();
        }
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
//		getActivity().invalidateOptionsMenu();
	}

	@Override
	public void afterTextChanged(Editable s) {
	}
}
