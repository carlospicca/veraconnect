/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */
package uy.com.antel.veracorreo.resource;

import java.net.URISyntaxException;

import ch.boye.httpclientandroidlib.impl.client.CloseableHttpClient;
import uy.com.antel.veracorreo.webdav.DavMultiget;

public class CardDavAddressBook extends RemoteCollection<Contact> {
	//private final static String TAG = "veracorreo.CardDavAddressBook";
	
	@Override
	protected String memberContentType() {
		return "text/vcard";
	}
	
	@Override
	protected DavMultiget.Type multiGetType() {
		return DavMultiget.Type.ADDRESS_BOOK;
	}

	@Override
	protected Contact newResourceSkeleton(String name, String ETag) {
		return new Contact(name, ETag);
	}
	

	public CardDavAddressBook(CloseableHttpClient httpClient, String baseURL, String user, String password, boolean preemptiveAuth) throws URISyntaxException {
		super(httpClient, baseURL, user, password, preemptiveAuth);
	}
}
