/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.syncadapter.adapters;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;

import uy.com.antel.veracorreo.R;
import uy.com.antel.veracorreo.resource.customRows.AccountRowItem;

/**
 * Created by carlospicca on 8/14/14.
 */
public class AccountRowAdapter extends ArrayAdapter<AccountRowItem> {

    private final Context context;
    private final ArrayList<AccountRowItem> itemsArrayList;
    private SharedPreferences settings;

    public AccountRowAdapter(Context context, ArrayList<AccountRowItem> itemsArrayList, SharedPreferences settings) {

        super(context, R.layout.account_row, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
        this.settings = settings;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.account_row, parent, false);

        // 3. Get the two text view from the rowView
        TextView nameLabelView = (TextView) rowView.findViewById(R.id.account_name_row);
        ImageView pencilImageView = (ImageView) rowView.findViewById(R.id.pencil);
        ImageView errorImageView = (ImageView) rowView.findViewById(R.id.errorImage);

        // 4. Set the text for textView
        nameLabelView.setText(itemsArrayList.get(position).getName());
        if (itemsArrayList.get(position).getType() == 1)
            pencilImageView.setImageResource(R.drawable.pencil);
        else
            nameLabelView.setTextColor(Color.parseColor("#FF727272"));

        errorImageView.setImageResource(R.drawable.error_sync);

        if (restorePreferences(itemsArrayList.get(position).getName()))
            errorImageView.setVisibility(View.VISIBLE);
        else
            errorImageView.setVisibility(View.INVISIBLE);

        // 5. return rowView
        return rowView;
    }

    public ArrayList<AccountRowItem> getItemsArrayList() {
        return itemsArrayList;
    }

    private boolean restorePreferences(String name) {
        HashSet<String> accountErrors = (HashSet<String>) settings.getStringSet("accountProblems", null);
        if (accountErrors != null) {
            for (String account : accountErrors) {
                String accountName = account.substring(0, account.indexOf("|"));
                String action = account.substring(account.indexOf("|") + 1, account.length());

                if (name.equals(accountName)) {
                    if (action.equals("Y"))
                        return true;
                    else
                        return false;
                }
            }
        }

        return false;
    }
}
