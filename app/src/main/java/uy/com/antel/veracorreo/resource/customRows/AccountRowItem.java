/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.resource.customRows;

/**
 * Created by carlospicca on 8/14/14.
 */
public class AccountRowItem {

    private String name;
    private int type;
    private int error;

    public AccountRowItem(String name, int type) {
        super();
        this.name = name;
        this.type = type;
        this.error = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
