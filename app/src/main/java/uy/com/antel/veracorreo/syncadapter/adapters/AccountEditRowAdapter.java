/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.syncadapter.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import uy.com.antel.veracorreo.R;
import uy.com.antel.veracorreo.resource.customRows.AccountEditRowItem;

/**
 * Created by carlospicca on 8/14/14.
 */
public class AccountEditRowAdapter extends ArrayAdapter<AccountEditRowItem> {

    private final Context context;
    private final ArrayList<AccountEditRowItem> itemsArrayList;

    public AccountEditRowAdapter(Context context, ArrayList<AccountEditRowItem> itemsArrayList) {

        super(context, R.layout.account_edit_row, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.account_edit_row, parent, false);

        // 3. Get the two text view from the rowView
        TextView nameLabelView = (TextView) rowView.findViewById(R.id.edit_item_row);
        ImageView alertImageView = (ImageView) rowView.findViewById(R.id.edit_item_image);

        // 4. Set the text for textView
        nameLabelView.setText(itemsArrayList.get(position).getName());
        if (itemsArrayList.get(position).getType() != 1)
            nameLabelView.setTextColor(Color.parseColor("#FF727272"));

        //set alarm image
        if (itemsArrayList.get(position).getError() == 1)
            alertImageView.setImageResource(R.drawable.error_sync);

        // 5. return rowView
        return rowView;
    }

    public ArrayList<AccountEditRowItem> getItemsArrayList() {
        return itemsArrayList;
    }
}
