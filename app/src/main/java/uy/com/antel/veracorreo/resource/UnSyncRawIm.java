/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.resource;

import java.io.Serializable;

/**
 * Created by carlospicca on 8/25/14.
 */
public class UnSyncRawIm implements Serializable {
    private String name;
    private int type;
    private String protocol;

    public UnSyncRawIm(String name, int type, String protocol) {
        this.name = name;
        this.type = type;
        this.protocol = protocol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
}
