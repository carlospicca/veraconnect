/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.syncadapter.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import uy.com.antel.veracorreo.R;
import uy.com.antel.veracorreo.resource.customRows.UnSyncContactRowItem;

import java.util.ArrayList;


/**
 * Created by carlospicca on 8/17/14.
 */
public class UnSyncContactRowAdapter extends ArrayAdapter<UnSyncContactRowItem> {

    private final Context context;
    private final ArrayList<UnSyncContactRowItem> itemsArrayList;

    public UnSyncContactRowAdapter(Context context, ArrayList<UnSyncContactRowItem> itemsArrayList) {

        super(context, R.layout.account_edit_row, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.unsync_contact_row, parent, false);

        // 3. Get the two text view from the rowView
        TextView nameLabelView = (TextView) rowView.findViewById(R.id.unsync_contact_item_row);
        ImageView checkBoxImageView = (ImageView) rowView.findViewById(R.id.unsync_contact_item_image);

        // 4. Set the text for textView
        nameLabelView.setText(itemsArrayList.get(position).getName());
        if (itemsArrayList.get(position).getType() >= -1)
            if (itemsArrayList.get(position).isSelected())
                checkBoxImageView.setImageResource(R.drawable.checkbox_checked);
            else
                checkBoxImageView.setImageResource(R.drawable.checkbox_empty);

        nameLabelView.setTextColor(Color.parseColor("#FF727272"));

        // 5. return rowView
        return rowView;
    }

    public ArrayList<UnSyncContactRowItem> getItemsArrayList() {
        return itemsArrayList;
    }
}