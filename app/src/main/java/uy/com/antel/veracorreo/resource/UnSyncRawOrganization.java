/*
 * ******************************************************************************
 *  * Copyright (c) 2014. Carlos Picca.
 *  * TW @carlospicca
 *  * Linware
 *  *****************************************************************************
 */

package uy.com.antel.veracorreo.resource;

import java.io.Serializable;

/**
 * Created by carlospicca on 8/24/14.
 */
public class UnSyncRawOrganization implements Serializable {
    private String company;
    private int type;
    private String title;

    public UnSyncRawOrganization(String company, int type, String title) {
        this.company = company;
        this.type = type;
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
