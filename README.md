# VERA CORREO - ANTEL #

### What is this repository for? ###

* This is an Android app for [Vera Correo](https://correo.vera.com.uy). You can synchronise your contacts and calendars with your mail account on veracorreo.
* Version 1.1.17

### How do I get set up? ###

* Deploy it on your phone and you're ready to go! 

### Who do I talk to? ###

* Repo owner: Carlos Picca. Copyright 2014.
* You can contact me on twitter @CarlosPicca